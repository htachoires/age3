/* FIRST RANDOM MAP - by DodgeMan - Version 1.0 */

include "mercenaries.xs";

void main(void) {

	rmSetStatusText("",0.01);

	int mapSize = 600;

	if(cNumberNonGaiaPlayers > 4) {
		mapSize = 650;
	}
	
	rmSetMapSize(mapSize, mapSize);
	
	rmSetMapType("land");
	rmSetMapType("caribbean");
	rmSetMapType("water");

	rmSetBaseTerrainMix("caribbean grass");

	rmSetLightingSet("caribbean");

	rmSetSeaLevel(-3.0);
	rmSetSeaType("caribbean coast");
	rmTerrainInitialize("water");
	
	chooseMercs();
	
	/* Classes can be added here: */
	int playerClass = rmDefineClass("playerClass");
	int socketClass = rmDefineClass("socketClass");
	int islandClass = rmDefineClass("islandClass");
	int islandMiddleClass = rmDefineClass("islandMiddleClass");
	int forestClass = rmDefineClass("forestClass");

	/* Contraints can be added here: */
	int avoidTownCenter = rmCreateTypeDistanceConstraint("avoid Town Center", "townCenter", 20.0);
	int avoidPlayerSpawn = rmCreateClassDistanceConstraint("avoid player spawn", playerClass, 70.0);
	int nuggetPlayerConstraint = rmCreateClassDistanceConstraint("nuggets stay away from players a lot", playerClass, 40.0);

	int islandAvoidOtherIsland = rmCreateClassDistanceConstraint("islands avoid each other medium", islandClass, 50.0);
	int islandAvoidMiddleIsland = rmCreateClassDistanceConstraint("islands avoid each other small", islandClass, 40.0);

	int avoidImpassableLandSmall = rmCreateTerrainDistanceConstraint("avoid impassable land small", "land", false, 7.0);
	int avoidImpassableLandMedium = rmCreateTerrainDistanceConstraint("avoid impassable land medium", "land", false, 15.0);
	int avoidImpassableLandLong = rmCreateTerrainDistanceConstraint("avoid impassable land long", "land", false, 27.0);

	int avoidTradeRoute = rmCreateTradeRouteDistanceConstraint("trade route", 6.0);
	int avoidTradeRoutePlayer = rmCreateTradeRouteDistanceConstraint("trade route player", 30.0);
	int avoidSocket = rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 8.0);
	int avoidSocketPlayer = rmCreateClassDistanceConstraint("socket avoidance", rmClassID("socketClass"), 10.0);

    // New extra stuff for water spawn point avoidance.
	int flagLand = rmCreateTerrainDistanceConstraint("flag vs land", "land", true, 30.0);
	int flagSpawn = rmCreateClassDistanceConstraint("flag vs land of player", playerClass, 30.0);
	int flagVsFlag = rmCreateTypeDistanceConstraint("flag avoid same", "HomeCityWaterSpawnFlag", 15);

	// Resource constraint

	int forestConstraint = rmCreateClassDistanceConstraint("forest vs. forest", rmClassID("forestClass"), 40.0);

	int fishVsFishID = rmCreateTypeDistanceConstraint("fish v fish", "FishMahi", 20.0);
	int fishLand = rmCreateTerrainDistanceConstraint("fish land", "land", true, 25.0);
	int whaleVsWhaleID = rmCreateTypeDistanceConstraint("whale v whale", "HumpbackWhale", 80.0);
	int whaleLand = rmCreateTerrainDistanceConstraint("whale land", "land", true, 50.0);

	int avoidMine = rmCreateTypeDistanceConstraint("mine avoid mine", "mine", 40.0);
	int avoidTree = rmCreateTypeDistanceConstraint("tree avoid tree", "tree", 20.0);
	int avoidHunt = rmCreateTypeDistanceConstraint("resource avoid resource", "resource", 10.0);
	int avoidNugget = rmCreateTypeDistanceConstraint("nugget avoid nugget", "abstractNugget", 50.0);
	int avoidAll = rmCreateTypeDistanceConstraint("avoid all", "all", 8.0);

	rmSetStatusText("", 0.10);

	float continentSize = 0.11;
	float continentXLocation = 0.2;
	float continentZLocation = 0.8;

	float contientCoherence = 0.60;
	float contientCoherenceTopBot = 0.50;
	float contientSmooth = 40.0;

	float continentMinBlobs = 35.0;
	float continentMaxBlobs = 40.0;
	float continentMinDistBlobs = 30.0;
	float continentMaxDistBlobs = 50.0;

	int continentLeft = rmCreateArea("continentLeft");
	rmAddAreaToClass(continentLeft, islandClass);
	rmAddAreaConstraint(continentLeft, islandAvoidOtherIsland);

	rmSetAreaSize(continentLeft, continentSize, continentSize);
	rmSetAreaLocation(continentLeft, continentXLocation, continentZLocation);
	rmSetAreaMix(continentLeft, "caribbean grass");

	rmSetAreaMinBlobs(continentLeft, continentMinBlobs);
	rmSetAreaMaxBlobs(continentLeft, continentMaxBlobs);
	rmSetAreaMinBlobDistance(continentLeft, continentMinDistBlobs);
	rmSetAreaMaxBlobDistance(continentLeft, continentMaxDistBlobs);

	rmSetAreaBaseHeight(continentLeft, 0.0);
	rmSetAreaCoherence(continentLeft, contientCoherence);
	rmSetAreaSmoothDistance(continentLeft, contientSmooth);
	rmSetAreaHeightBlend(continentLeft, 2.0);
	
		rmSetAreaElevationType(continentLeft, cElevTurbulence);
		rmSetAreaElevationVariation(continentLeft, 2.0);
		rmSetAreaElevationMinFrequency(continentLeft, 0.09);
		rmSetAreaElevationOctaves(continentLeft, 3);
		rmSetAreaElevationPersistence(continentLeft, 0.2);
		rmSetAreaElevationNoiseBias(continentLeft, 1);

	rmBuildArea(continentLeft);

	int continentRight = rmCreateArea("continentRight");
	rmAddAreaToClass(continentRight, islandClass);
	rmAddAreaConstraint(continentRight, islandAvoidOtherIsland);

	rmSetAreaMinBlobs(continentRight, continentMinBlobs);
	rmSetAreaMaxBlobs(continentRight, continentMaxBlobs);
	rmSetAreaMinBlobDistance(continentRight, continentMinDistBlobs);
	rmSetAreaMaxBlobDistance(continentRight, continentMaxDistBlobs);

	rmSetAreaSize(continentRight, continentSize, continentSize);
	rmSetAreaLocation(continentRight, continentZLocation, continentXLocation);	//invert for symmetric
	rmSetAreaMix(continentRight, "caribbean grass");
	rmSetAreaBaseHeight(continentRight, 0.0);
	rmSetAreaCoherence(continentRight, contientCoherence);
	rmSetAreaSmoothDistance(continentRight, contientSmooth);
	rmSetAreaHeightBlend(continentRight, 2.0);

		rmSetAreaElevationType(continentRight, cElevTurbulence);
		rmSetAreaElevationVariation(continentRight, 2.0);
		rmSetAreaElevationMinFrequency(continentRight, 0.09);
		rmSetAreaElevationOctaves(continentRight, 3);
		rmSetAreaElevationPersistence(continentRight, 0.2);
		rmSetAreaElevationNoiseBias(continentRight, 1);

	rmBuildArea(continentRight);

	int continentMiddle = rmCreateArea("continentMiddle");
	rmAddAreaToClass(continentMiddle, islandMiddleClass);
	rmAddAreaConstraint(continentMiddle, islandAvoidMiddleIsland);

	rmSetAreaSize(continentMiddle, 0.03, 0.03);
	rmSetAreaLocation(continentMiddle, 0.5, 0.5);
	rmSetAreaMix(continentMiddle, "caribbean grass");

	rmSetAreaMinBlobs(continentMiddle, 20);
	rmSetAreaMaxBlobs(continentMiddle, 25);
	rmSetAreaMinBlobDistance(continentMiddle, 20);
	rmSetAreaMaxBlobDistance(continentMiddle, 40);

	rmSetAreaBaseHeight(continentMiddle, 0.0);
	rmSetAreaCoherence(continentMiddle, 0.2);
	rmSetAreaSmoothDistance(continentMiddle, 0.0);
	rmSetAreaHeightBlend(continentMiddle, 2.0);

		rmSetAreaElevationType(continentMiddle, cElevTurbulence);
		rmSetAreaElevationVariation(continentMiddle, 2.0);
		rmSetAreaElevationMinFrequency(continentMiddle, 0.09);
		rmSetAreaElevationOctaves(continentMiddle, 3);
		rmSetAreaElevationPersistence(continentMiddle, 0.2);
		rmSetAreaElevationNoiseBias(continentMiddle, 1);

	rmBuildArea(continentMiddle);

	rmSetStatusText("", 0.20);
	
	if (cNumberTeams == 2) {
		rmSetPlacementTeam(0);
		rmPlacePlayersLine(0.1, 0.7, 0.3, 0.9, 10, 0);

		rmSetPlacementTeam(-1);
		rmPlacePlayersLine( 0.9, 0.3, 0.7, 0.1, 10, 0); //mirror other land symmetric
	} else {
		rmPlacePlayer(1, 0.3, 0.9);
		rmPlacePlayer(7, 0.2, 0.8);
		rmPlacePlayer(3, 0.1, 0.7);
		rmPlacePlayer(5, 0.3, 0.7);
		rmPlacePlayer(4, 0.9, 0.3);
		rmPlacePlayer(8, 0.8, 0.2);
		rmPlacePlayer(2, 0.7, 0.1);
		rmPlacePlayer(6, 0.7, 0.3);
	}

	//STARTING UNITS and RESOURCES DEFS

	int startingUnits = rmCreateStartingUnitsObjectDef(5.0);
	rmSetObjectDefMinDistance(startingUnits, 7.0);
	rmSetObjectDefMaxDistance(startingUnits, 12.0);
		rmAddObjectDefConstraint(startingUnits, avoidTradeRoute);

	int sartingBerryID = rmCreateObjectDef("starting berries");
	rmAddObjectDefItem(sartingBerryID, "BerryBush", 2, 5.0);
	rmSetObjectDefMinDistance(sartingBerryID, 11.0);
	rmSetObjectDefMaxDistance(sartingBerryID, 12.0);
		rmAddObjectDefConstraint(sartingBerryID, avoidTradeRoute);
		rmAddObjectDefConstraint(sartingBerryID, avoidSocket);

	int startingTreeID = rmCreateObjectDef("starting trees");
	rmAddObjectDefItem(startingTreeID, "treeCaribbean", 4, 5.0);
	rmSetObjectDefMinDistance(startingTreeID, 12.0);
	rmSetObjectDefMaxDistance(startingTreeID, 18.0);
		rmAddObjectDefConstraint(startingTreeID, avoidTradeRoute);
		rmAddObjectDefConstraint(startingTreeID, avoidSocket);

	int startingMineID = rmCreateObjectDef("starting mines");
	rmAddObjectDefItem(startingMineID, "mineGold", 1, 0.0);
	rmSetObjectDefMinDistance(startingMineID, 15.0);
	rmSetObjectDefMaxDistance(startingMineID, 20.0);
		rmAddObjectDefConstraint(startingMineID, avoidTradeRoute);
		rmAddObjectDefConstraint(startingMineID, avoidSocket);

	int startingHuntID = rmCreateObjectDef("starting hunt");
	rmAddObjectDefItem(startingHuntID, "turkey", 7, 6.0);
	rmSetObjectDefCreateHerd(startingHuntID, true);
	rmSetObjectDefMinDistance(startingHuntID, 15.0);
	rmSetObjectDefMaxDistance(startingHuntID, 20.0);
		rmAddObjectDefConstraint(startingHuntID, avoidTradeRoute);
		rmAddObjectDefConstraint(startingHuntID, avoidSocket);

	int startingHuntID2 = rmCreateObjectDef("starting hunt2");
	rmAddObjectDefItem(startingHuntID2, "capybara", 7, 6.0);
	rmSetObjectDefCreateHerd(startingHuntID2, true);
	rmSetObjectDefMinDistance(startingHuntID2, 22.0);
	rmSetObjectDefMaxDistance(startingHuntID2, 28.0);
		rmAddObjectDefConstraint(startingHuntID2, avoidTradeRoute);
		rmAddObjectDefConstraint(startingHuntID2, avoidSocket);

	int startingHuntID3 = rmCreateObjectDef("starting hunt3");
	rmAddObjectDefItem(startingHuntID3, "turkey", 7, 6.0);
	rmSetObjectDefCreateHerd(startingHuntID3, true);
	rmSetObjectDefMinDistance(startingHuntID3, 29.0);
	rmSetObjectDefMaxDistance(startingHuntID3, 35.0);
		rmAddObjectDefConstraint(startingHuntID3, avoidTradeRoute);
		rmAddObjectDefConstraint(startingHuntID3, avoidSocket);

	/* Now we start a loop for every player: */
	for(i = 1; <= cNumberNonGaiaPlayers)
	{
		int playerSpawn = rmCreateArea("Player" + i);
		rmSetAreaCoherence(playerSpawn, 1.0);
		rmSetAreaSize(playerSpawn, 0.02, 0.02);
		rmSetAreaCoherence(playerSpawn, contientCoherence);
		rmSetAreaMix(playerSpawn, "caribbean grass");
		rmSetAreaBaseHeight(playerSpawn, 0);
		rmAddAreaToClass(playerSpawn, playerClass);
		rmSetAreaHeightBlend(playerSpawn, 2);
		rmSetAreaLocPlayer(playerSpawn, i);
		rmSetPlayerArea(i, playerSpawn);
		rmBuildArea(playerSpawn);
	}
	
	for(i = 1; <= cNumberNonGaiaPlayers)
	{
		int townCenterID = rmCreateObjectDef("Starting Towncenter" + i);
		rmAddObjectDefItem(townCenterID,"TownCenter", 1, 0.0);
		
		rmPlaceObjectDefAtLoc(townCenterID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i), 1);

		rmPlaceObjectDefAtLoc(startingUnits, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(sartingBerryID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingMineID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingTreeID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingTreeID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingHuntID, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingHuntID2, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		rmPlaceObjectDefAtLoc(startingHuntID3, i, rmPlayerLocXFraction(i), rmPlayerLocZFraction(i));
		// Water flag
		// I understand a bit but not really
		int waterFlagID = rmCreateObjectDef("HC water flag " + i);
		rmAddObjectDefItem(waterFlagID, "HomeCityWaterSpawnFlag", 1, 0.0);
			rmAddClosestPointConstraint(flagVsFlag);
			rmAddClosestPointConstraint(flagLand);
			rmAddClosestPointConstraint(flagSpawn);
		vector TCLocation = rmGetUnitPosition(rmGetUnitPlacedOfPlayer(townCenterID, i));
		vector closestPoint = rmFindClosestPointVector(TCLocation, rmXFractionToMeters(1.0));

		rmPlaceObjectDefAtLoc(waterFlagID, i, rmXMetersToFraction(xsVectorGetX(closestPoint)), rmZMetersToFraction(xsVectorGetZ(closestPoint)));
		rmClearClosestPointConstraints();
	}

	rmSetStatusText("", 0.40);

	int continentTop = rmCreateArea("continentTop");
	rmAddAreaToClass(continentTop, islandClass);
	rmAddAreaConstraint(continentTop, islandAvoidOtherIsland);
	rmAddAreaConstraint(continentTop, avoidPlayerSpawn);

	rmSetAreaSize(continentTop, 0.12, 0.12);
	rmSetAreaLocation(continentTop, 1, 1);
	rmSetAreaMix(continentTop, "caribbean grass");

	rmSetAreaMinBlobs(continentTop, continentMinBlobs);
	rmSetAreaMaxBlobs(continentTop, continentMaxBlobs);
	rmSetAreaMinBlobDistance(continentTop, continentMinDistBlobs);
	rmSetAreaMaxBlobDistance(continentTop, continentMaxDistBlobs);

	rmSetAreaBaseHeight(continentTop, 0.0);
	rmSetAreaCoherence(continentTop, contientCoherenceTopBot);
	rmSetAreaSmoothDistance(continentTop, contientSmooth);
	rmSetAreaHeightBlend(continentTop, 2.0);

		rmSetAreaElevationType(continentTop, cElevTurbulence);
		rmSetAreaElevationVariation(continentTop, 2.0);
		rmSetAreaElevationMinFrequency(continentTop, 0.09);
		rmSetAreaElevationOctaves(continentTop, 3);
		rmSetAreaElevationPersistence(continentTop, 0.2);
		rmSetAreaElevationNoiseBias(continentTop, 1);

	rmBuildArea(continentTop);

	int continentBot = rmCreateArea("continentBot");
	rmAddAreaToClass(continentBot, islandClass);
	rmAddAreaConstraint(continentBot, islandAvoidOtherIsland);
	rmAddAreaConstraint(continentBot, avoidPlayerSpawn);

	rmSetAreaSize(continentBot, 0.13, 0.13);
	rmSetAreaLocation(continentBot, 0, 0);
	rmSetAreaMix(continentBot, "caribbean grass");

	rmSetAreaMinBlobs(continentBot, continentMinBlobs);
	rmSetAreaMaxBlobs(continentBot, continentMaxBlobs);
	rmSetAreaMinBlobDistance(continentBot, continentMinDistBlobs);
	rmSetAreaMaxBlobDistance(continentBot, continentMaxDistBlobs);

	rmSetAreaBaseHeight(continentBot, 0.0);
	rmSetAreaCoherence(continentBot, contientCoherenceTopBot);
	rmSetAreaSmoothDistance(continentBot, contientSmooth);
	rmSetAreaHeightBlend(continentBot, 2.0);

		rmSetAreaElevationType(continentBot, cElevTurbulence);
		rmSetAreaElevationVariation(continentBot, 2.0);
		rmSetAreaElevationMinFrequency(continentBot, 0.09);
		rmSetAreaElevationOctaves(continentBot, 3);
		rmSetAreaElevationPersistence(continentBot, 0.2);
		rmSetAreaElevationNoiseBias(continentBot, 1);

	rmBuildArea(continentBot);

	//TRADE ROUTE
	int socketID = rmCreateObjectDef("sockets for Trade Posts");
	rmAddObjectDefToClass(socketID, socketClass);
	rmAddObjectDefItem(socketID, "SocketTradeRoute", 1, 0.0);
	rmSetObjectDefAllowOverlap(socketID, true);
	rmSetObjectDefMinDistance(socketID, 0.0);
	rmSetObjectDefMaxDistance(socketID, 6.0);

	int tradeRouteID = rmCreateTradeRoute();
	rmSetObjectDefTradeRouteID(socketID, tradeRouteID);
	rmAddTradeRouteWaypoint(tradeRouteID, 1, 0.65);
	rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.8, 0.7, 5, 5);
	rmAddRandomTradeRouteWaypoints(tradeRouteID, 0.7, 0.87, 5, 5);
	rmAddTradeRouteWaypoint(tradeRouteID, 0.65, 1);

	rmBuildTradeRoute(tradeRouteID, "dirt");

	if(cNumberNonGaiaPlayers  > 4) {
		vector socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.13, 0.19));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc2);
		
		socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.32, 0.38));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc2);

		socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.56, 0.64));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc2);

		socketLoc2 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.79, 0.87));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc2);
	} else {
		vector socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.17, 0.23));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);

		socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.47, 0.53));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);

		socketLoc1 = rmGetTradeRouteWayPoint(tradeRouteID, rmRandFloat(0.72, 0.78));
		rmPlaceObjectDefAtPoint(socketID, 0, socketLoc1);
	}

	rmSetStatusText("", 0.20);

	//NATIVE VILLAGES
	int caribVillageType = rmRandInt(1, 5);

	int caribVillageAID = rmCreateGrouping("native village A", "native carib village 0" + caribVillageType);
	rmSetGroupingMinDistance(caribVillageAID, 0.0);
	rmSetGroupingMaxDistance(caribVillageAID, 15.0);
		rmAddGroupingConstraint(caribVillageAID, avoidTradeRoute);
		rmAddGroupingConstraint(caribVillageAID, avoidPlayerSpawn);
	rmPlaceGroupingAtLoc(caribVillageAID, 0, 0.12, 0.3);
	
	int caribVillageType3 = rmRandInt(1, 5);
	int caribVillageAID3 = rmCreateGrouping("native village B", "native carib village 0" + caribVillageType3);
	rmSetGroupingMinDistance(caribVillageAID3, 0.0);
	rmSetGroupingMaxDistance(caribVillageAID3, 15.0);
		rmAddGroupingConstraint(caribVillageAID3, avoidTradeRoute);
		rmAddGroupingConstraint(caribVillageAID3, avoidPlayerSpawn);
	rmPlaceGroupingAtLoc(caribVillageAID3, 0, 0.2, 0.2);

	int caribVillageType2 = rmRandInt(1, 5);
	int caribVillageAID2 = rmCreateGrouping("native village C", "native carib village 0" + caribVillageType2);
	rmSetGroupingMinDistance(caribVillageAID2, 0.0);
	rmSetGroupingMaxDistance(caribVillageAID2, 15.0);
		rmAddGroupingConstraint(caribVillageAID2, avoidTradeRoute);
		rmAddGroupingConstraint(caribVillageAID2, avoidPlayerSpawn);
	rmPlaceGroupingAtLoc(caribVillageAID2, 0, 0.3, 0.12);

	rmSetStatusText("", 0.50);

	// MIDDLE MAP

	int bigNuggetClass = rmDefineClass("big nugget class");
	int bigNuggetConstraintSmall = rmCreateClassDistanceConstraint("outpost avoid big nugget medium", bigNuggetClass, 10);
	int bigNuggetConstraintExtraSmall = rmCreateClassDistanceConstraint("outpost avoid big nugget small", bigNuggetClass, 4);
	int bigNuggetConstraintMedium = rmCreateClassDistanceConstraint("outpost avoid big nugget medium", bigNuggetClass, 35);
	int avoidOutpost = rmCreateTypeDistanceConstraint("outpost avoid outpost", "outpost", 50.0);
	int avoidPirate = rmCreateTypeDistanceConstraint("outpost avoid pirate", "pirate", 8.0);
	
	int caribVillageTypeMiddle = rmRandInt(1, 5);
	int testGrouping = rmCreateGrouping("test grouping", "native carib village 0" + caribVillageTypeMiddle);
	rmSetGroupingMinDistance(testGrouping, 0.02);
	rmSetGroupingMaxDistance(testGrouping, 0.07);
		rmAddGroupingConstraint(testGrouping, avoidImpassableLandLong);
	rmPlaceGroupingInArea(testGrouping, 0, continentMiddle);

	int BigNugget = rmCreateObjectDef("nugget big"); 
	rmAddObjectDefItem(BigNugget, "NuggetPirate", 1, 0.0);
	rmAddObjectDefToClass(BigNugget, bigNuggetClass);
	rmSetNuggetDifficulty(4, 4);
		rmAddObjectDefConstraint(BigNugget, avoidImpassableLandLong);
	rmPlaceObjectDefInArea(BigNugget, 0, continentMiddle, 1);
	
	int crateOfFood = rmCreateObjectDef("crateOfFood");
	rmAddObjectDefItem(crateOfFood, "CrateofFood", 1, 0.0);
		rmAddObjectDefConstraint(crateOfFood, avoidImpassableLandMedium);
		rmAddObjectDefConstraint(crateOfFood, avoidAll);
	rmPlaceObjectDefInArea(crateOfFood, 0, continentMiddle, rmRandInt(5, 6));

	int CrateofCoin = rmCreateObjectDef("CrateofCoin");
	rmAddObjectDefItem(CrateofCoin, "CrateofCoin", 1, 0.0);
		rmAddObjectDefConstraint(CrateofCoin, avoidImpassableLandMedium);
		rmAddObjectDefConstraint(CrateofCoin, avoidAll);
	rmPlaceObjectDefInArea(CrateofCoin, 0, continentMiddle, rmRandInt(5, 6));

	int CrateofWood = rmCreateObjectDef("CrateofWood");
	rmAddObjectDefItem(CrateofWood, "CrateofWood", 1, 0.0);
		rmAddObjectDefConstraint(CrateofWood, avoidImpassableLandMedium);
		rmAddObjectDefConstraint(CrateofWood, avoidAll);
	rmPlaceObjectDefInArea(CrateofWood, 0, continentMiddle, rmRandInt(5, 6));

	rmSetStatusText("", 0.80);

	// RANDOM MINES
	int randomMineID = rmCreateObjectDef("random mine");
	rmAddObjectDefItem(randomMineID, "mine", 1, 0.0);
		rmAddObjectDefConstraint(randomMineID, avoidMine);
		rmAddObjectDefConstraint(randomMineID, avoidAll);
		rmAddObjectDefConstraint(randomMineID, avoidTradeRoute);
		rmAddObjectDefConstraint(randomMineID, avoidSocket);
		rmAddObjectDefConstraint(randomMineID, avoidImpassableLandMedium);
		rmAddObjectDefConstraint(randomMineID, avoidTownCenter);
	rmPlaceObjectDefInArea(randomMineID, 0, continentLeft, cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomMineID, 0, continentTop, cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomMineID, 0, continentBot, cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomMineID, 0, continentRight, cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomMineID, 0, continentMiddle, 3);

	// RANDOM TREES
	int randomTreeID = rmCreateObjectDef("random tree");
	rmAddObjectDefItem(randomTreeID, "treeCaribbean", 1, 0.0);
		rmAddObjectDefConstraint(randomTreeID, avoidTradeRoute);
		rmAddObjectDefConstraint(randomTreeID, avoidSocket);
		rmAddObjectDefConstraint(randomTreeID, avoidImpassableLandSmall);
		rmAddObjectDefConstraint(randomTreeID, avoidTownCenter);
	rmPlaceObjectDefInArea(randomTreeID, 0, continentLeft, 10 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomTreeID, 0, continentRight, 10 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomTreeID, 0, continentBot, 12 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomTreeID, 0, continentTop, 12 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomTreeID, 0, continentMiddle, 10 * cNumberNonGaiaPlayers);

	// RANDOM HUNT
	int randomHuntID = rmCreateObjectDef("random hunt");
	rmAddObjectDefItem(randomHuntID, "capybara", rmRandInt(12, 16), 8.0);
		rmAddObjectDefConstraint(randomHuntID, avoidAll);
		rmAddObjectDefConstraint(randomHuntID, avoidTradeRoute);
		rmAddObjectDefConstraint(randomHuntID, avoidTownCenter);
	rmPlaceObjectDefInArea(randomHuntID, 0, continentLeft, 2 + 1.05 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID, 0, continentRight, 2 + 1.05 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID, 0, continentBot, 2 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID, 0, continentTop, 2 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID, 0, continentMiddle, 4);

	int randomHuntID2 = rmCreateObjectDef("random hunt 2");
	rmAddObjectDefItem(randomHuntID2, "turkey", rmRandInt(12, 16), 8.0);
		rmAddObjectDefConstraint(randomHuntID2, avoidAll);
		rmAddObjectDefConstraint(randomHuntID2, avoidTradeRoute);
		rmAddObjectDefConstraint(randomHuntID2, avoidTownCenter);
	rmPlaceObjectDefInArea(randomHuntID2, 0, continentLeft, 2 + 1.05 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID2, 0, continentRight, 2 + 1.05 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID2, 0, continentBot, 2 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomHuntID2, 0, continentTop, 2 * cNumberNonGaiaPlayers);

	//RANDOM NUGGET
	int randomNuggetID = rmCreateObjectDef("nugget hard");
	rmAddObjectDefItem(randomNuggetID, "Nugget", 1, 0.0);
	rmSetNuggetDifficulty(1, 3);
		rmAddObjectDefConstraint(randomNuggetID, avoidNugget);
		rmAddObjectDefConstraint(randomNuggetID, avoidAll);
		rmAddObjectDefConstraint(randomNuggetID, avoidTradeRoute);
		rmAddObjectDefConstraint(randomNuggetID, avoidImpassableLandMedium);
		rmAddObjectDefConstraint(randomNuggetID, avoidTownCenter);
	rmPlaceObjectDefInArea(randomNuggetID, 0, continentLeft, 2 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomNuggetID, 0, continentRight, 2 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomNuggetID, 0, continentBot, 3 * cNumberNonGaiaPlayers);
	rmPlaceObjectDefInArea(randomNuggetID, 0, continentTop, 3 * cNumberNonGaiaPlayers);

	// FORESTS
	int forestTreeID = 0;
	int numTries = 10 * cNumberNonGaiaPlayers;
	//numTries = 0;
	int failCount = 0;

	for(i = 0; < numTries)
	{   
		int forest = rmCreateArea("forest " + i);
		rmSetAreaSize(forest, rmAreaTilesToFraction(150), rmAreaTilesToFraction(400));
		rmAddAreaToClass(forest, rmClassID("classForest"));
		rmSetAreaForestType(forest, "caribbean palm forest");

		rmSetAreaForestDensity(forest, 0.6);
		rmSetAreaForestUnderbrush(forest, 0.0);
		rmSetAreaForestClumpiness(forest, 0.4);

		rmSetAreaMinBlobs(forest, 1);
		rmSetAreaMaxBlobs(forest, 5);
		rmSetAreaMinBlobDistance(forest, 16.0);
		rmSetAreaMaxBlobDistance(forest, 40.0);

		rmSetAreaCoherence(forest, 0.4);
		rmSetAreaSmoothDistance(forest, 10);

		rmAddAreaConstraint(forest, forestConstraint);
		rmAddAreaConstraint(forest, avoidImpassableLandSmall);
		rmAddAreaConstraint(forest, avoidTownCenter);
		rmAddAreaConstraint(forest, avoidTradeRoute);
		rmAddAreaConstraint(forest, avoidSocket);

		if(rmBuildArea(forest) == false)
         {
            failCount++;
            if(failCount == 5) break;
         }
         else
            failCount=0; 
	} 

	//RANDOM FISHES
	int fishID = rmCreateObjectDef("fish Mahi");
	rmAddObjectDefItem(fishID, "FishMahi", 1, 0.0);
	rmSetObjectDefMinDistance(fishID, 0.0);
	rmSetObjectDefMaxDistance(fishID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(fishID, fishVsFishID);
	rmAddObjectDefConstraint(fishID, fishLand);
	rmPlaceObjectDefAtLoc(fishID, 0, 0.5, 0.5, 15 * cNumberNonGaiaPlayers);

	int fish2ID = rmCreateObjectDef("fish Tarpon");
	rmAddObjectDefItem(fish2ID, "FishTarpon", 1, 0.0);
	rmSetObjectDefMinDistance(fish2ID, 0.0);
	rmSetObjectDefMaxDistance(fish2ID, rmXFractionToMeters(0.5));
	rmAddObjectDefConstraint(fish2ID, fishLand);
	rmPlaceObjectDefAtLoc(fish2ID, 0, 0.5, 0.5, 20 * cNumberNonGaiaPlayers);

	//RANDOM WHALES
	int whaleID = rmCreateObjectDef("whale");
	rmAddObjectDefItem(whaleID, "HumpbackWhale", 1, 0.0);
	rmSetObjectDefMinDistance(whaleID, 0.0);
	rmSetObjectDefMaxDistance(whaleID, rmXFractionToMeters(0.45));
	rmAddObjectDefConstraint(whaleID, whaleVsWhaleID);
	rmAddObjectDefConstraint(whaleID, whaleLand);
	rmPlaceObjectDefAtLoc(whaleID, 0, 0.5, 0.5, 2 * cNumberNonGaiaPlayers);

	rmSetStatusText("", 1.00);
}